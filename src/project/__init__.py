from flask import Flask
from redis import Redis

app = Flask(__name__)
redis = Redis(host='my_redis', port=6379)

@app.route("/")
def index():
    return "хай !"

@app.route("/counter")
def counter():
    redis.incr('hits')
    return 'This Compose/Flask demo has been viewed %s time(s).' % redis.get('hits')


