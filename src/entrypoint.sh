#!/bin/sh

if [ "$FLASK_ENV" = "dev" ]
then
    python manage.py run -h 0.0.0.0
elif [ "$FLASK_ENV" = "prod" ]
then
    gunicorn --bind 0.0.0.0:5000 manage:app
fi
